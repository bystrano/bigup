<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bigup_titre' => 'Big Upload',
	'bouton_enlever' => 'Enlever',

	// C
	'cfg_charger_public' => 'Script dans l’espace public',
	'cfg_charger_public_case' => 'Charger les scripts dans l’espace public',
	'cfg_max_file_size' => 'Taille maximum des fichiers',
	'cfg_max_file_size_explication' => 'Taille maximum des fichiers (en Mb).',
	'cfg_titre_parametrages' => 'Paramétrages de Big Upload',

	// D
	'deposer_vos_fichiers_ici' => 'Déposer vos fichiers ici',
	'deposer_votre_fichier_ici' => 'Déposer votre fichier ici',

	// E
	'erreur_de_transfert' => 'Erreur de transfert.',
	'erreur_taille_max' => 'Le fichier ne doit pas dépasser @taille@',
	'erreur_type_fichier' => 'Type de fichier incorrect !',
	'erreur_probleme_survenu' => 'Un problème est survenu…',

	// O
	'ou' => 'ou', 

	// T
	'televerser' => 'Téléverser',
	'titre_page_configurer_bigup' => 'Configurer Big Upload',

);
